import unittest
from substring_checking import Substring_checking
from read_file import Read
w=Read("input.txt").readFromFile()

class Test_substring(unittest.TestCase):
    def test_empty_string(self):
        words=w[0].split(",")
        substring_checker=Substring_checking()
        self.assertEqual(substring_checker.isSubstring(words[0].strip(),words[1].strip()),-1)

    def test_string(self):
        words=w[1].spl
        substring_checker = Substring_checking()
        self.assertEquals(substring_checker.isSubstring(words[0].strip(),words[1].strip()),1)